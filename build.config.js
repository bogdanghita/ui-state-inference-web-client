module.exports = {

	build_dir : "build",
	deploy_dir : "deploy/public",
	dev_deploy_dir : "dev/public",

	conf_deploy_dir : "deploy",
	dev_conf_deploy_dir : "dev",
	
	app_files : {
		js : [ 'src/app/app.js', 'src/app/directives/*.js','src/app/*.js', 'src/app/controllers/*.js', 'src/app/services/*.js'],
		css : [
			'src/app/style/*.css'
		],
		img : [
			'src/app/img/*.*'
		],
		fonts : [
			'src/app/fonts/*.*'
		],
		partials: [
			'src/app/partials/*.html'
		],
		index : [
			'src/app/index.html'
		],
		conf:[
			'package.json',
			'config.js.template',
			'nginx.conf.template'
			]
	},
	vendor_files : {
		js : [
			'vendor/jquery/dist/jquery.min.js',
			'vendor/jquery-ui/ui/minified/jquery-ui.min.js',
			'vendor/angular/angular.min.js',
			'vendor/bootstrap/dist/js/bootstrap.min.js',
			'vendor/angular-bootstrap/ui-bootstrap-tpls.min.js',
			'vendor/angular-route/angular-route.min.js',
      'vendor/angular-material/angular-material.min.js',
			'vendor/angular-animate/angular-animate.min.js',
      'vendor/angular-aria/angular-aria.min.js',
      'vendor/underscore/underscore-min.js',
      'vendor/angular-underscore-module/angular-underscore-module.js',
      'vendor/angular-socket-io/socket.min.js',
      'vendor/socket.io-client/dist/socket.io.min.js',
			'vendor/highcharts-release/highstock.js',
			'vendor/highcharts-release/modules/exporting.js',
			'vendor/highcharts-release/highcharts-more.js',
			'vendor/highmaps-release/modules/map.js'
		],
		css : [
			'vendor/font-awesome/css/font-awesome.min.css',
			'vendor/bootstrap/dist/css/bootstrap.min.css',
      'vendor/angular-material/angular-material.min.css',
		],
		fonts : [
			'vendor/bootstrap/dist/fonts/*',
			'vendor/font-awesome/fonts/*'
		],
		images :[
		]
	}
}
