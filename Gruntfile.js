module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-html2js');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-exec');
	grunt.loadNpmTasks('grunt-template');
	grunt.loadNpmTasks('grunt-replace');

var buildConfig = require('./build.config.js');

var storeHgId = function(stderr, stdout){
	grunt.option('hgVersion', stdout.trim());
}

var storeHgRepo = function(stderr, stdout){
	grunt.option('hgRepo', stdout.trim());
}

var taskConfig = {
		exec: {
			hgid: {
				cmd: 'git rev-list HEAD --max-count=1',
                callback: storeHgId
			},
			hgrepo: {
				cmd: 'git rev-list HEAD --count',
                callback: storeHgRepo
			}
		},
		pkg: grunt.file.readJSON('package.json'),
		html2js: {
			options : {
				base : 'src/app',
				module : 'partialModule'
			},
			main:{
				src : ['<%= app_files.partials %>'],
				dest: '<%= build_dir %>/dash_html.js'
			}
		},
		replace: {
		    target: {
		        options: {
		            patterns: [
		                // {
		                //     match: 'HGREPONUMBER',
		                //     replacement: '<%= grunt.option("hgRepo") %>'
		                // }
		            ]
		        },
		        src: [(grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/js/config.js')],
		        dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/js/config.js')
		    }
		},
		template: {
			index_prod: {
				options: {
					data: {
						// jsVersion	: 'js/ui-state-inference-web-client.min.js?v=<%= grunt.option("hgVersion")%>',
						// cssVersion 	: 'css/ui-state-inference-web-client.min.css?v=<%= grunt.option("hgVersion")%>',
						// cfgVersion 	: 'js/config.js?v=<%= grunt.option("hgVersion")%>',
						// hgRepo 		: '<%= grunt.option("hgRepo")%>'
						jsVersion	: 'js/ui-state-inference-web-client.min.js',
						cssVersion 	: 'css/ui-state-inference-web-client.min.css',
						cfgVersion 	: 'js/config.js'
					}
				},
				files: {
					'<%= deploy_dir %>/index.html': ['<%= app_files.index %>']
				}
			},
			index_dev :{
				options : {
					data : {
						jsVersion 	: 'js/ui-state-inference-web-client.min.js',
						cssVersion 	: 'css/ui-state-inference-web-client.min.css',
						cfgVersion 	: 'js/config.js',
						// hgRepo 		: '<%= grunt.option("hgRepo")%>'
					}
				},
				files:[{
					src: ['<%= app_files.index %>'],
					dest:grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>/index.html":"<%= deploy_dir %>/index.html"
					
				}]
			}
		},		
		copy : {
			server_confs : {
				files:[
					{
						src: ['<%= app_files.conf %>'],
						dest: grunt.option("dist")=="dev"?"<%= dev_conf_deploy_dir %>":"<%= conf_deploy_dir %>",
						cwd: '.',
						flatten: true,
						expand:true
					}
				]
			},
			app_confs:{
				files:[
					{
						src: ['src/app/config.js'],
						dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/js'),
						cwd: '.',
						flatten: true,
						expand:true
					}
				]
			},
			vendor_js:{
				files:[
					{
						src: ['<%= vendor_files.js %>'],
						dest : '<%= build_dir %>/vendor',
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			vendor_css:{
				files:[
					{
						src: ['<%= vendor_files.css %>'],
						dest : '<%= build_dir %>',
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			vendor_fonts :{
				files:[
					{
						src: [ '<%= vendor_files.fonts %>' ],
						dest : (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/fonts'),
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			vendor_images:{
				files:[
					{
						src: [ '<%= vendor_files.images %>' ],
						dest : (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/css/images'),
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			app_js :{
				files : [
					{
						src : ['<%= app_files.js %>'],
						dest : '<%= build_dir %>/app',
						cwd: '.',
						flatten: true,
						expand: true 
					}
				]
			},
			app_css :{
				files : [
					{
						src : ['<%= app_files.css %>'],
						dest : '<%= build_dir %>',
						cwd: '.',
						flatten: true,
						expand: true 
					}
				]
			},
			app_img : {
				files:[
					{
						src: [ '<%= app_files.img %>' ],
						dest : (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/img'),
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			app_fonts : {
				files:[
				{
						src: [ '<%= app_files.fonts %>' ],
						dest : (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/fonts'),
						cwd: '.',
						flatten: true,
						expand: true
					}
				]
			},
			/*app_index : {
				src : '<%= app_files.index%>',
				dest : '<%= deploy_dir%>',
				cwd: '.',
				flatten: true,
				expand: true
			},*/
			vendor_workers: {
				src: ["vendor/ace-builds/src-min-noconflict/worker-javascript.js", 
					  "vendor/ace-builds/src-min-noconflict/worker-json.js"],
				dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/js'),
				cwd: '.',
				flatten: true,
				expand: true
			},
			vendor_snippets: {
				src: ["vendor/ace-builds/src-min-noconflict/snippets/javascript.js"],
				dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/snippets'),
				cwd: ".",
				flatten: true,
				expand: true
			}
		},
		uglify: {
			options: {
				mangle: false,
				compress: {
		        	drop_console: true
				}
			},
			dev: {
				src: ['<%= app_files.js%>','<%= build_dir %>/dash_html.js'],
				dest: '<%= build_dir%>/<%= pkg.name %>.js'
			},
			prod: {
				src: ['<%= app_files.js%>','<%= build_dir %>/dash_html.js', '!src/app/config.js'],
				dest: '<%= build_dir%>/<%= pkg.name %>.js'
			},
		},
		concat: {
			options: {
				// define a string to put between each file in the concatenated output
				// process: function(src, filepath) {
				// 	return '// Source: ' + filepath + '\n' +
				// 	src.replace(/[ \t]*('use strict'|"use strict");?\s*/g, '$1');
				// 	},
					// separator : ';\n'
				},
			all_js: {
				// the files to concatenate
				src: ['<%=vendor_files.js %>','<%= build_dir %>/<%= pkg.name %>.js'],
				// the location of the resulting JS file
				dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/js/<%= pkg.name %>.min.js')
			},
			css :{
				src: ['<%= build_dir %>/*.css'],
				// the location of the resulting JS file
				dest: (grunt.option("dist")=="dev"?"<%= dev_deploy_dir %>":"<%= deploy_dir %>").concat('/css/<%= pkg.name %>.min.css')
			},
			app_js:{
				src: ['<%= app_files.js%>','<%= build_dir %>/dash_html.js', '!src/app/config.js'],
				dest: '<%= build_dir%>/<%= pkg.name %>.js'
			}
		},
		clean:{
			prod: [
				'<%= build_dir %>',
				'deploy'],
			dev :[
				'<%= build_dir %>',
				'dev']
		},
		watch: {
			scripts: {
				files: ['<%= app_files.js%>',
						'<%= app_files.css%>',
						'<%= app_files.index%>',
						'<%= app_files.partials%>',
						'<%= app_files.img%>'],
				tasks: ['dev'],
				options: {
					spawn: false,
					livereload: true
				},
			},
		}
	};
	// Load the plugin that provides the "uglify" task.
	grunt.initConfig( grunt.util._.extend( taskConfig, buildConfig ) );
	
	grunt.registerTask('prod', [/*'exec:hgid',
								'exec:hgrepo',*/
								'clean:prod',
								'copy:app_img',
								'copy:app_fonts',
								'copy:vendor_fonts',								
								'template:index_prod',
								'copy:app_css',
								'copy:vendor_css',
								'copy:vendor_images',
								'html2js',
								'uglify:prod',
								'concat:all_js',
								'concat:css',
								'copy:server_confs',
								'copy:app_confs',
								'replace:target',
								'copy:vendor_workers',
								'copy:vendor_snippets']);
	grunt.registerTask('dev', [	/*'exec:hgid',
								'exec:hgrepo',*/
								'clean:dev',
								'copy:app_img',
								'copy:app_fonts',
								'copy:vendor_fonts',
								'template:index_dev',
								'copy:app_css',
								'copy:vendor_css',
								'copy:vendor_images',
								'html2js',						
								'concat:app_js',
								'concat:all_js',
								'concat:css',
								'copy:server_confs',
								'copy:app_confs',
								'replace:target',
								'copy:vendor_workers',
								'copy:vendor_snippets']);
};
