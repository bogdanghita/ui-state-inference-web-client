usiaApp.directive('highchartsBasic', function($filter) {
  return {
    restrict: 'A',
    transclude: true,
    scope: {
      data: '=',
      zones: '=',
      tooltip: '=',
      ymin: '=',
      ymax: '=',
      title: '=',
      ytitle: '=',
      xaxistype: '='
    },
    link: link
  };

  function link(scope, element) {

    Highcharts.Basic = function (a, b, c) {
      var hasRenderToArg = typeof a === 'string' || a.nodeName;
      var options = arguments[hasRenderToArg ? 1 : 0];
      var defaultOptions = {
        chart: {
          renderTo: (options.chart && options.chart.renderTo) || this,
          // backgroundColor: null,
          // borderWidth: 0,
          // type: 'area',
          // margin: [2, 0, 2, 0],
          // width: 150,
          height: 250,
          style: {
            // overflow: 'visible'
          },
          // skipClone: true,
          zoomType: 'x'
        },
        title: {
          // text: ''
        },
        credits: {
          enabled: false
        },
        xAxis: {
          labels: {
            // enabled: false
          },
          title: {
            // text: null
          },
          // startOnTick: false,
          // endOnTick: false,
          // tickPositions: []
        },
        yAxis: {
          // endOnTick: false,
          // startOnTick: false,
          labels: {
              // enabled: false
          },
          title: {
              // text: null
          },
          // tickPositions: [0]
        },
        legend: {
          enabled: false
        },
        tooltip: {
          // backgroundColor: null,
          // borderWidth: 0,
          // shadow: false,
          // useHTML: true,
          // hideDelay: 0,
          // shared: true,
          // padding: 0,
          // positioner: function (w, h, point) {
          //   return { x: point.plotX - w / 2, y: point.plotY - h };
          // }
        },
        plotOptions: {
          series: {
            animation: false,
            lineWidth: 1,
            // shadow: false,
            states: {
              hover: {
                lineWidth: 1
              }
            },
            marker: {
              radius: 1,
              states: {
                hover: {
                  radius: 2
                }
              }
            },
            fillOpacity: 0.25
          },
          column: {
            negativeColor: '#910000',
            borderColor: 'silver'
          }
        },
        exporting: {
          enabled: false
        }
      };

      options = Highcharts.merge(defaultOptions, options);

      return hasRenderToArg ?
        new Highcharts.Chart(a, options, c) :
        new Highcharts.Chart(options, b);
    };
  
    scope.$watch('[data, tooltip, ymin, ymax, title]', drawChart, true);

    function drawChart(){
      var options = {
        chart: {},
        title: {},
        xAxis: {},
        yAxis: {},
        legend: {},
        tooltip: {},
        series: [{
            data: scope.data,
            zones: scope.zones
        }]
      };
      if(scope.tooltip){
        options.tooltip = scope.tooltip;
      }
      if((scope.ymin != undefined) && (scope.ymax != undefined)){
        options.yAxis = {
          min: scope.ymin,
          max: scope.ymax
        };
      }
      if(scope.title){
        options.title.text = scope.title;
      }
      if(scope.ytitle){
        options.yAxis.title = {
          text: scope.ytitle
        }
      }
      if(scope.xaxistype){
        options.xAxis.type = scope.xaxistype;
      }
      $(element).highcharts('Basic', options);
    }
  }
});
