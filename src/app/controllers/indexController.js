usiaControllers.controller("indexController", ["$scope", "socketIO", function ($scope, socketIO) {
  console.log("[indexController]");

  $scope.paused = false;

  $scope.smData = {};
  $scope.ongoingSmData = {};

  $scope.appData = {};

  $scope.$on('destroy', function(){
    sendMessage('output:unsubscribe', {});
  });

  socketIO.on('connect', function(){
    console.log("[connect]");
    handleConnect();
  });
  socketIO.on('disconnect', function(){
    console.log("[disconnect]");
    handleDisconnect();
  });
  socketIO.on('output:sm', function(data){
    // console.log("[output:sm]");
    handleSmDataInput(data);
  });

  function handleConnect(){
    sendMessage('output:subscribe', {});
  }

  function handleDisconnect(){}
  
  function handleSmDataInput(data){
    var smData = $scope.paused ? $scope.ongoingSmData : $scope.smData;
    var appKey = getKey(data.device_id, data.app_info);

    if(!(appKey in $scope.appData)){
      $scope.appData[appKey] = {
        device_name: data.device_name,
        app_name: data.app_info.name,
        package_name: data.app_info.packageName
      };
    }
    $scope.appData[appKey].pid = data.app_info.pid;

    if(!(appKey in smData)){
      smData[appKey] = [];
    }

    smData[appKey] = smData[appKey].concat(data.values);
    var overflowLength = smData[appKey].length - MAX_CHART_POINTS;
    if(overflowLength > 0){
      smData[appKey].splice(0, overflowLength);
    }
  }

  function sendMessage(tag, data){
    socketIO.emit(tag, data, function(err, res){
      console.log("[handleConnect]");
    });
  }

  $scope.togglePause = function(){
    $scope.paused = !$scope.paused;
    if($scope.paused){
      $scope.ongoingSmData = angular.fromJson(angular.toJson($scope.smData));
    }
    else{
      $scope.smData = angular.fromJson(angular.toJson($scope.ongoingSmData));
    }
  }

  function getKey(device_id, app_info){
    return device_id + app_info.packageName;
  }

}]);
