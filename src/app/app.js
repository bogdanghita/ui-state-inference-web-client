var usiaApp = angular.module('usiaApp', [
    'ngRoute',
    'ui.bootstrap',
    'usiaControllers',
    'partialModule',
    'ngMaterial',
    'underscore',
    'btford.socket-io'
]);
var usiaControllers = angular.module('usiaControllers', []);

usiaApp.factory('socketIO', function(socketFactory) {
  var ioSocket = null;
  try{
    ioSocket = io.connect(SERVER_URL);
  }
  catch(err){
    console.log("[socketIO] Unable to connect to %s. error=%s", SERVER_URL, JSON.stringify(err));
  }

  return socketFactory({
    ioSocket: ioSocket
  });
});

usiaApp.config(['$routeProvider','$locationProvider',
  function($routeProvider , $locationProvider) {
    $routeProvider.
      // when('/', {
      //   templateUrl: 'partials/testPage1.html',
      //   controller: 'testController1',
      // }).
      otherwise({redirectTo: '/'});
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  }]);

usiaApp.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('indigo')
    .accentPalette('pink')
    .warnPalette('red')
    .backgroundPalette('grey');
});
